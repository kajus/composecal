package com.cowcode.composecal.day

import java.time.LocalDate

/**
 * Contains basic info about displayed day
 *
 * @param "date" today's local date
 * @param "isCurrentDay" check if is today
 * @param "isFromCurrentMonth" check if from current month
 */
interface Day {
    val date: LocalDate
    val isCurrentDay: Boolean
    val isFromCurrentMonth: Boolean
}