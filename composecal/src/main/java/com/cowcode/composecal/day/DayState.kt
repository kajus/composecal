package com.cowcode.composecal.day

import androidx.compose.runtime.Stable
import com.cowcode.composecal.selection.EmptySelectionState
import com.cowcode.composecal.selection.SelectionState

typealias NonSelectableDayState = DayState<EmptySelectionState>

/**
 * Contains information about current selection as well as date of rendered day
 */
@Stable
data class DayState<T : SelectionState>(
    private val day: Day,
    val selectionState: T,
) : Day by day
