package com.cowcode.composecal.day

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.cowcode.composecal.selection.SelectionState
import java.time.LocalDate

/**
 * This is the default implementation for displaying the content of a day in a calendar.
 * It allows for different appearances for days from the current month,
 * adjacent months, the current day, and the selected day.
 *
 * @param selectionColor sets the border color for when a day is selected
 * @param currentDayColor  parameter sets the color of the content for the current date
 * @param onClick callback function that is triggered when a day is clicked
 */
@Composable
fun <T : SelectionState> DefaultDay(
    state: DayState<T>,
    modifier: Modifier = Modifier,
    selectionColor: Color = MaterialTheme.colorScheme.secondary,
    currentDayColor: Color = MaterialTheme.colorScheme.primary,
    onClick: (LocalDate) -> Unit = {},
) {
    val date = state.date
    val selectionState = state.selectionState
    val isSelected = selectionState.isDateSelected(date)

    Card(
        modifier = modifier
            .aspectRatio(1f)
            .padding(3.dp),
        elevation = if (state.isFromCurrentMonth) CardDefaults.cardElevation(4.dp)
        else CardDefaults.cardElevation(0.dp),
        border = if (state.isCurrentDay) BorderStroke(2.dp, currentDayColor) else null,
        colors = if (isSelected) CardDefaults.cardColors(selectionColor)
                    else CardDefaults.cardColors(MaterialTheme.colorScheme.surface)
    ) {
        Box(
            modifier = Modifier
                .clickable {
                    onClick(date)
                    selectionState.onDateSelected(date)
                }
                .fillMaxSize(),
            contentAlignment = Alignment.Center,
        ) {

            Text(
                text = date.dayOfMonth.toString(),
            )
        }
    }
}
