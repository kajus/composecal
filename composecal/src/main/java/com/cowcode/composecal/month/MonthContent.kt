package com.cowcode.composecal.month


import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.snapping.rememberSnapFlingBehavior
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import com.cowcode.composecal.day.DayState
import com.cowcode.composecal.header.MonthState
import com.cowcode.composecal.selection.SelectionState
import com.cowcode.composecal.week.WeekContent
import com.cowcode.composecal.week.getWeeks
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth

internal const val DaysOfWeek = 7

@OptIn(ExperimentalFoundationApi::class)
@Composable
@Suppress("LongMethod", "LongParameterList")
internal fun <T : SelectionState> MonthPager(
    initialMonth: YearMonth,
    showAdjacentMonths: Boolean,
    selectionState: T,
    monthState: MonthState,
    daysOfWeek: List<DayOfWeek>,
    today: LocalDate,
    modifier: Modifier = Modifier,
    dayContent: @Composable BoxScope.(DayState<T>) -> Unit,
    weekHeader: @Composable BoxScope.(List<DayOfWeek>) -> Unit,
    monthContainer: @Composable (content: @Composable (PaddingValues) -> Unit) -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()

    val listState = rememberLazyListState(
        initialFirstVisibleItemIndex = StartIndex,
    )
//  val flingBehavior = rememberSnapFlingBehavior(
//    lazyListState = listState,
//    snapOffsetForItem = SnapOffsets.Start,
//    springAnimationSpec = SnapFlingBehavior.SpringAnimationSpec,
//    decayAnimationSpec = rememberSplineBasedDecay(),
//    snapIndex = coerceSnapIndex,
//  )
    val flingBehavior = rememberSnapFlingBehavior(lazyListState = listState)


    val monthListState = remember {
        MonthListState(
            coroutineScope = coroutineScope,
            initialMonth = initialMonth,
            monthState = monthState,
            listState = listState,
        )
    }

    LazyRow(
        modifier = modifier.testTag("MonthPager"),
        state = listState,
        flingBehavior = flingBehavior,
        verticalAlignment = Alignment.Top,
    ) {
        items(PagerItemCount) { index ->
            MonthContent(
                modifier = Modifier.fillParentMaxWidth(),
                showAdjacentMonths = showAdjacentMonths,
                selectionState = selectionState,
                currentMonth = monthListState.getMonthForPage(index),
                today = today,
                daysOfWeek = daysOfWeek,
                dayContent = dayContent,
                weekHeader = weekHeader,
                monthContainer = monthContainer,
            )
        }
    }
}

@Composable
internal fun <T : SelectionState> MonthContent(
    showAdjacentMonths: Boolean,
    selectionState: T,
    currentMonth: YearMonth,
    daysOfWeek: List<DayOfWeek>,
    today: LocalDate,
    modifier: Modifier = Modifier,
    dayContent: @Composable BoxScope.(DayState<T>) -> Unit,
    weekHeader: @Composable BoxScope.(List<DayOfWeek>) -> Unit,
    monthContainer: @Composable (content: @Composable (PaddingValues) -> Unit) -> Unit,
) {
    Column {
        Box(
            modifier = modifier
                .wrapContentHeight(),
            content = { weekHeader(daysOfWeek) },
        )

        monthContainer { paddingValues ->
            Column(
                modifier = modifier
                    .padding(paddingValues)
            ) {
                currentMonth.getWeeks(
                    includeAdjacentMonths = showAdjacentMonths,
                    firstDayOfTheWeek = daysOfWeek.first(),
                    today = today,
                ).forEach { week ->
                    WeekContent(
                        week = week,
                        selectionState = selectionState,
                        dayContent = dayContent,

                    )
                }
            }
        }
    }
}

//@OptIn(ExperimentalSnapperApi::class)
//private val coerceSnapIndex: (SnapperLayoutInfo, startIndex: Int, targetIndex: Int) -> Int =
//  { _, startIndex, targetIndex ->
//    targetIndex
//      .coerceIn(startIndex - 1, startIndex + 1)
//  }
