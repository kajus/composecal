package com.cowcode.composecal.selection

/**
 * Selection modes for the [SelectionState]
 * None - no selection allowed
 * Single - only one date selected
 * Multiple - multiple dates can be selected
 * Period - period can be selected
 */
enum class SelectionMode {
    None,
    Single,
    Multiple,
    Period,
    ;
}
