package com.cowcode.composecal.week

import androidx.compose.runtime.Immutable
import com.cowcode.composecal.day.Day

@Immutable
internal data class Week(
    val isFirstWeekOfTheMonth: Boolean = false,
    val days: List<Day>,
)
